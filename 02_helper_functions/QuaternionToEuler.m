function [pitch, roll, yaw] = QuaternionToEuler(q0, q1, q2, q3)
% Converts a Quaternion to Euler angles

[psi, theta,phi] = quat2angle([q0.Data, q1.Data, q2.Data, q3.Data], 'ZYX');
% 
% phi = atan2(2*(q0.Data.*q1.Data + q2.Data.* q3.Data), q0.Data.^2 - q1.Data.^2 - q2.Data.^2 + q3.Data.^2);
% theta = asin(-2*(q1.Data.*q3.Data - q0.Data.*q2.Data));
% psi = atan2(2*(q1.Data.*q2.Data + q0.Data.* q3.Data), q0.Data.^2 + q1.Data.^2 - q2.Data.^2 - q3.Data.^2);

yaw = timeseries(psi, q0.Time);
pitch = timeseries(theta, q0.Time);
roll = timeseries(phi, q0.Time);